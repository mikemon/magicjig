package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"magicjig/gitlab"
	"magicjig/state"
	"magicjig/webhook"
)

func ticketHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log.Printf("Lookup merge requests for %s.\n", vars["issue"])
	fmt.Fprintf(w, "Merge requests for %s", vars["issue"])
	w.Header().Set("Content-Type", "text/html")
}

type webhookHandler struct{}

func (_ webhookHandler) CommentEvent(ce *webhook.CommentEvent) {
	// XXX
}

func (_ webhookHandler) MergeRequestEvent(mre *webhook.MergeRequestEvent) {
	// XXX
}

func main() {
	err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	r := mux.NewRouter()
	r.HandleFunc("/ticket/{issue}", ticketHandler).Methods("GET")

	gitlab.Init(config.GitlabURL, secrets.GitlabToken)
	state.Init()
	state.Load()
	webhook.Init(r, webhookHandler{})

	http.Handle("/", r)
	//log.Fatal(http.ListenAndServe(":4567", nil))
}
