package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

var (
	configFilename  = flag.String("config", "config.json", "config filename")
	secretsFilename = flag.String("secrets", "secrets.json", "secrets filename")

	config = struct {
		GitlabURL string `json:"gitlab_url"`
	}{}

	secrets = struct {
		GitlabToken string `json:"gitlab_token"`
	}{}
)

func loadJSON(filename string, v interface{}) error {
	file, err := os.Open(filename)
	if err == nil {
		decoder := json.NewDecoder(file)
		err = decoder.Decode(v)
		if err != nil {
			return err
		}
	}
	return nil
}

func getenv(key, def string) string {
	val, ok := os.LookupEnv(key)
	if ok {
		return val
	}
	return def
}

func loadConfig() error {
	flag.Parse()

	err := loadJSON(*configFilename, &config)
	if err != nil {
		return fmt.Errorf("load config: %s", err)
	}

	err = loadJSON(*secretsFilename, &secrets)
	if err != nil {
		return fmt.Errorf("load secrets: %s", err)
	}
	secrets.GitlabToken = getenv("GITLAB_TOKEN", secrets.GitlabToken)

	return nil
}
