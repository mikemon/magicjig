package gitlab

import (
	"fmt"

	api "github.com/xanzy/go-gitlab"
)

var (
	client *api.Client
)

type Project struct {
	ID   int
	Name string
}

type MergeRequest struct {
	ID           int
	IID          int
	ProjectID    int
	Title        string
	TargetBranch string
	SourceBranch string
	State        string
}

type Commit struct {
	ID              string
	MergeRequestIID int
	Message         string
}

func GetProject(id int) (*Project, error) {
	// XXX
	return nil, nil
}

func ListProjects() ([]*Project, error) {
	projects, _, err := client.Projects.ListProjects(nil)
	if err != nil {
		return nil, fmt.Errorf("list projects: %s", err)
	}

	var ret []*Project
	for _, proj := range projects {
		ret = append(ret, &Project{ID: proj.ID, Name: proj.Name})
	}
	return ret, nil
}

func (p *Project) ListMergeRequests() ([]*MergeRequest, error) {
	mreqs, _, err := client.MergeRequests.ListProjectMergeRequests(p.ID, nil)
	if err != nil {
		return nil, fmt.Errorf("list merge requests: %s", err)
	}

	var ret []*MergeRequest
	for _, mreq := range mreqs {
		ret = append(ret,
			&MergeRequest{
				ID:           mreq.ID,
				IID:          mreq.IID,
				ProjectID:    p.ID,
				Title:        mreq.Title,
				TargetBranch: mreq.TargetBranch,
				SourceBranch: mreq.SourceBranch,
				State:        mreq.State,
			})
	}
	return ret, nil
}

func (p *Project) GetMergeRequest(id int) (*MergeRequest, error) {
	// XXX
	return nil, nil
}

func (mr *MergeRequest) ListCommits() ([]*Commit, error) {
	commits, _, err := client.MergeRequests.GetMergeRequestCommits(mr.ProjectID, mr.IID)
	if err != nil {
		return nil, fmt.Errorf("get merge request commits: %s", err)
	}

	var ret []*Commit
	for _, commit := range commits {
		ret = append(ret,
			&Commit{
				ID:              commit.ID,
				MergeRequestIID: mr.IID,
				Message:         commit.Message,
			})

	}
	return ret, nil
}

func Init(url, token string) {
	client = api.NewClient(nil, token)
	client.SetBaseURL(url)
}
