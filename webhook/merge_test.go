package webhook_test

import (
	"testing"

	"magicjig/webhook"
)

func TestMergeRequestEvent(t *testing.T) {
	cases := []struct {
		body []byte
		want webhook.MergeRequestEvent
		fail bool
	}{
		{
			body: []byte(`
{
	"object_kind": "merge_request",
	"object_attributes": {
		"iid": 23,
		"source_project_id": 31,
		"target_project_id": 31,
		"action": "open"
	}
}`),
			want: webhook.MergeRequestEvent{
				ID:        "23",
				ProjectID: 31,
				Action:    webhook.OpenMergeRequest,
			},
		},
		{
			body: []byte(`
{
	"object_kind": "merge_request",
	"object_attributes": {
		"iid": 23,
		"source_project_id": 32,
		"target_project_id": 31,
		"action": "open"
	}
}`),
			fail: true,
		},
		{
			body: []byte(`
{
	"object_kind": "merge_request",
	"object_attributes": {
		"iid": 23,
		"source_project_id": 31,
		"target_project_id": 31,
		"action": "no-action"
	}
}`),
			fail: true,
		},
	}

	for _, c := range cases {
		mre, err := webhook.MergeRequestUnmarshal(c.body)
		if c.fail {
			if err == nil {
				t.Errorf("MergeRequestUnmarshal(%q) did not fail", c.body)
			}
		} else {
			if err != nil {
				t.Errorf("MergeRequestUnmarshal(%q) failed with %s", c.body, err)
			} else if mre == nil {
				t.Errorf("MergeRequestUnmarshal(%q) returned nil", c.body)
			} else if *mre != c.want {
				t.Errorf("MergeRequestUnmarshal(%q) got %v want %v", c.body, *mre, c.want)
			}
		}
	}
}
