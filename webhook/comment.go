package webhook

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type CommentType int

const (
	MergeRequestComment CommentType = iota
	CommitComment
)

type CommentEvent struct {
	Type      CommentType
	ID        string
	ProjectID int
	Comment   string
	URL       string
}

const commentKind = "note"

func CommentUnmarshal(body []byte) (*CommentEvent, error) {
	type commit struct {
		ID string `json:"id"`
	}
	type mergeRequest struct {
		ID int `json:"id"`
	}
	var ce struct {
		Kind       string `json:"object_kind"`
		ProjectID  int    `json:"project_id"`
		Attributes struct {
			Comment string `json:"note"`
			Type    string `json:"noteable_type"`
			URL     string `json:"url"`
		} `json:"object_attributes"`
		Commit       *commit       `json:"commit"`
		MergeRequest *mergeRequest `json:"merge_request"`
	}

	err := json.Unmarshal(body, &ce)
	if err != nil {
		return nil, err
	}
	if ce.Kind != commentKind {
		return nil, fmt.Errorf("CommentUnmarshal: expected %q got %q", commentKind, ce.Kind)
	}
	if ce.Attributes.Type == "MergeRequest" {
		if ce.MergeRequest == nil {
			return nil, fmt.Errorf("CommentUnmarshal: missing merge_request object in JSON")
		}
		return &CommentEvent{
			Type:      MergeRequestComment,
			ID:        strconv.FormatInt(int64(ce.MergeRequest.ID), 10),
			ProjectID: ce.ProjectID,
			Comment:   ce.Attributes.Comment,
			URL:       ce.Attributes.URL,
		}, nil
	} else if ce.Attributes.Type == "Commit" {
		if ce.Commit == nil {
			return nil, fmt.Errorf("CommentUnmarshal: missing commit object in JSON")
		}
		return &CommentEvent{
			Type:      CommitComment,
			ID:        ce.Commit.ID,
			ProjectID: ce.ProjectID,
			Comment:   ce.Attributes.Comment,
			URL:       ce.Attributes.URL,
		}, nil
	}
	return nil, nil
}

func CommentHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("CommentHandler:", err)
		return
	}
	ce, err := CommentUnmarshal(body)
	if err != nil {
		log.Println("CommentHandler:", err)
		return
	}
	if ce != nil {
		go handlers.CommentEvent(ce)
	}
}
