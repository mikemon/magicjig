package webhook

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type MergeRequestAction int

const (
	OpenMergeRequest MergeRequestAction = iota
	CloseMergeRequest
)

type MergeRequestEvent struct {
	ID        string
	ProjectID int
	Action    MergeRequestAction
}

const mergeRequestKind = "merge_request"

func MergeRequestUnmarshal(body []byte) (*MergeRequestEvent, error) {
	var mre struct {
		Kind       string `json:"object_kind"`
		Attributes struct {
			ID            int    `json:"iid"`
			SourceProject int    `json:"source_project_id"`
			TargetProject int    `json:"target_project_id"`
			Action        string `'json:"action"`
		} `json:"object_attributes"`
	}

	err := json.Unmarshal(body, &mre)
	if err != nil {
		return nil, err
	}
	if mre.Kind != mergeRequestKind {
		return nil, fmt.Errorf("MergeRequestUnmarshal: expected %q got %q", mergeRequestKind,
			mre.Kind)
	}
	if mre.Attributes.SourceProject != mre.Attributes.TargetProject {
		return nil, fmt.Errorf(
			"MergeRequestUnmarshal: source_project_id(%d) != target_project_id(%d)",
			mre.Attributes.SourceProject, mre.Attributes.TargetProject)
	}

	var a MergeRequestAction
	switch mre.Attributes.Action {
	case "open":
		a = OpenMergeRequest
	case "close":
		a = CloseMergeRequest
	default:
		return nil, fmt.Errorf("MergeRequestUnmarshal: unexpected action: %s",
			mre.Attributes.Action)
	}
	return &MergeRequestEvent{
		ID:        strconv.FormatInt(int64(mre.Attributes.ID), 10),
		ProjectID: mre.Attributes.SourceProject,
		Action:    a,
	}, nil
}

func MergeRequestHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("MergeRequestHandler:", err)
		return
	}
	mre, err := MergeRequestUnmarshal(body)
	if err != nil {
		log.Println("MergeRequestHandler:", err)
		return
	}
	if mre != nil {
		go handlers.MergeRequestEvent(mre)
	}
}
