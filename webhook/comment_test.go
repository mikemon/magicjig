package webhook_test

import (
	"testing"

	"magicjig/webhook"
)

func TestCommentEvent(t *testing.T) {
	cases := []struct {
		body []byte
		want webhook.CommentEvent
		fail bool
	}{
		{
			body: []byte(`
{
	"object_kind": "note",
	"project_id": 7,
	"object_attributes": {
		"id": 307,
		"note": "Test comment",
		"noteable_type": "MergeRequest"
	},
	"merge_request": {
		"id": 34
	}
}`),
			want: webhook.CommentEvent{
				Type:      webhook.MergeRequestComment,
				ID:        "34",
				ProjectID: 7,
				Comment:   "Test comment",
			},
		},
		{
			body: []byte(`
{
	"object_kind": "note",
	"project_id": 7,
	"object_attributes": {
		"id": 307,
		"note": "Test comment",
		"noteable_type": "MergeRequest"
	}
}`),
			fail: true,
		},
		{
			body: []byte(`
{
	"object_kind": "note",
	"project_id": 7,
	"object_attributes": {
		"id": 307,
		"note": "Test comment",
		"noteable_type": "MergeRequest"
	},
}`), // note the trailing comma
			fail: true,
		},
		{
			body: []byte(`
{
	"object_kind": "note",
	"project_id": 7,
	"object_attributes": {
		"id": 307,
		"note": "Test comment",
		"noteable_type": "Commit"
	},
	"merge_request": {
		"id": 34
	}
}`),
			fail: true,
		},
		{
			body: []byte(`
{
	"object_kind": "note",
	"project_id": 7,
	"object_attributes": {
		"id": 307,
		"note": "Test comment",
		"noteable_type": "Commit"
	},
	"commit": {
		"id": "cfe32cf61b73a0d5e9f13e774abde7ff789b1660"
	}
}`),
			want: webhook.CommentEvent{
				Type:      webhook.CommitComment,
				ID:        "cfe32cf61b73a0d5e9f13e774abde7ff789b1660",
				ProjectID: 7,
				Comment:   "Test comment",
			},
		},
	}

	for _, c := range cases {
		ce, err := webhook.CommentUnmarshal(c.body)
		if c.fail {
			if err == nil {
				t.Errorf("CommentUnmarshal(%q) did not fail", c.body)
			}
		} else {
			if err != nil {
				t.Errorf("CommentUnmarshal(%q) failed with %s", c.body, err)
			} else if ce == nil {
				t.Errorf("CommentUnmarshal(%q) returned nil", c.body)
			} else if *ce != c.want {
				t.Errorf("CommentUnmarshal(%q) got %v want %v", c.body, *ce, c.want)
			}
		}
	}
}
