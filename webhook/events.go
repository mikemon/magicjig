package webhook

import (
	"github.com/gorilla/mux"
)

type Handlers interface {
	CommentEvent(ce *CommentEvent)
	MergeRequestEvent(mre *MergeRequestEvent)
}

var handlers Handlers

func Init(r *mux.Router, h Handlers) {
	handlers = h

	r.HandleFunc("/webhook", CommentHandler).Methods("POST").Headers("X-Gitlab-Event", "Note Hook")
	r.HandleFunc("/webhook", MergeRequestHandler).Methods("POST").Headers("X-Gitlab-Event",
		"Merge Request Hook")
}
