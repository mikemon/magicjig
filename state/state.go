package state

import (
	"sort"
	"sync"
)

var (
	issueToMergeRequests map[string]map[int]bool
	mergeRequestToIssues map[int][]string

	issueToCommits map[string]map[string]bool
	commitToIssues map[string][]string

	mergeRequestToCommits map[int][]string

	mutex = &sync.RWMutex{}
)

func Init() {
	issueToMergeRequests = map[string]map[int]bool{}
	mergeRequestToIssues = map[int][]string{}

	issueToCommits = map[string]map[string]bool{}
	commitToIssues = map[string][]string{}

	mergeRequestToCommits = map[int][]string{}
}

// MergeRequestsForIssue returns the merge requests associated with the specified issue.
func MergeRequestsForIssue(issue string) []int {
	mutex.RLock()
	defer mutex.RUnlock()
	mreqset, ok := issueToMergeRequests[issue]
	if !ok {
		return nil
	}
	var mreqs []int
	for id := range mreqset {
		mreqs = append(mreqs, id)
	}
	sort.Ints(mreqs)
	return mreqs
}

// IssuesForMergeRequest returns the issues associated with specified merge request.
func IssuesForMergeRequest(id int) []string {
	mutex.RLock()
	defer mutex.RUnlock()
	issues, _ := mergeRequestToIssues[id]
	return issues
}

func removedIssues(previous, issues []string) []string {
	var removed []string
	for _, prev := range previous {
		not_found := true
		for _, issue := range issues {
			if prev == issue {
				not_found = false
				break
			}
		}
		if not_found {
			removed = append(removed, prev)
		}
	}

	return removed
}

func copySortStrings(issues []string) []string {
	if issues == nil {
		return nil
	}
	nissues := make([]string, len(issues))
	copy(nissues, issues)
	sort.Strings(nissues)
	return nissues
}

// SetMergeRequestIssues sets the list of issues associated with a merge request.
func SetMergeRequestIssues(id int, issues []string) {
	mutex.Lock()
	defer mutex.Unlock()
	issues = copySortStrings(issues)
	if previous, ok := mergeRequestToIssues[id]; ok {
		for _, issue := range removedIssues(previous, issues) {
			if mreqset, ok := issueToMergeRequests[issue]; ok {
				delete(mreqset, id)
			}
		}
	}
	for _, issue := range issues {
		mreqset, ok := issueToMergeRequests[issue]
		if !ok {
			mreqset = map[int]bool{}
			issueToMergeRequests[issue] = mreqset
		}
		mreqset[id] = true
	}
	mergeRequestToIssues[id] = issues
}

// CommitsForIssue returns the commits associated with the specified issue. Only commits
// not associated with a merge request are returned.
func CommitsForIssue(issue string) []string {
	mutex.RLock()
	defer mutex.RUnlock()
	cset, ok := issueToCommits[issue]
	if !ok {
		return nil
	}
	var commits []string
	for id := range cset {
		commits = append(commits, id)
	}
	sort.Strings(commits)
	return commits
}

// IssuesForCommit returns the issues associated with the specified commit.
func IssuesForCommit(id string) []string {
	mutex.RLock()
	defer mutex.RUnlock()
	issues, _ := commitToIssues[id]
	return issues
}

// SetCommitIssues sets the list of issues associated with a commit.
func SetCommitIssues(id string, issues []string) {
	mutex.Lock()
	defer mutex.Unlock()
	issues = copySortStrings(issues)
	if previous, ok := commitToIssues[id]; ok {
		for _, issue := range removedIssues(previous, issues) {
			if cset, ok := issueToCommits[issue]; ok {
				delete(cset, id)
			}
		}
	}
	for _, issue := range issues {
		cset, ok := issueToCommits[issue]
		if !ok {
			cset = map[string]bool{}
			issueToCommits[issue] = cset
		}
		cset[id] = true
	}
	commitToIssues[id] = issues
}

// CommitsForMergeRequest returns the list of commits associated with a merge request.
func CommitsForMergeRequest(id int) []string {
	mutex.RLock()
	defer mutex.RUnlock()
	commits, _ := mergeRequestToCommits[id]
	return commits
}

// SetMergeRequestCommits sets the list of commits associated with a merge request.
func SetMergeRequestCommits(id int, commits []string) {
	mutex.Lock()
	defer mutex.Unlock()
	mergeRequestToCommits[id] = copySortStrings(commits)
}
