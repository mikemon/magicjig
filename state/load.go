package state

import (
	"fmt"
	"log"

	"magicjig/gitlab"
)

func Load() {
	projects, err := gitlab.ListProjects()
	if err != nil {
		log.Fatal(err)
	}
	for _, proj := range projects {
		fmt.Printf("\nID: %d Name: %s\n", proj.ID, proj.Name)

		mreqs, err := proj.ListMergeRequests()
		if err != nil {
			log.Println(err)
			continue
		}
		for _, mreq := range mreqs {
			fmt.Printf("ID: %d IID: %d ProjectID: %d Title: %s\n", mreq.ID, mreq.IID,
				mreq.ProjectID, mreq.Title)
			fmt.Printf("TargetBranch: %s SourceBranch: %s State: %s\n",
				mreq.TargetBranch, mreq.SourceBranch, mreq.State)

			commits, err := mreq.ListCommits()
			if err != nil {
				log.Println(err)
				continue
			}
			for _, commit := range commits {
				fmt.Printf("================\nID: %s\nMessage: %s\n================\n", commit.ID,
					commit.Message)
			}
		}
	}
}
