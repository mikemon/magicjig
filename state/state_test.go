package state_test

import (
	"reflect"
	"testing"

	"magicjig/state"
)

type stepType int

const (
	initState stepType = iota
	mergeRequestsForIssue
	issuesForMergeRequest
	setMergeRequestIssues
	commitsForIssue
	issuesForCommit
	setCommitIssues
	commitsForMergeRequest
	setMergeRequestCommits
)

type testStep struct {
	step    stepType
	issue   string
	issues  []string
	mreqs   []int
	mrid    int
	commits []string
	cid     string
}

func test(which string, steps []testStep, t *testing.T) {
	for i, step := range steps {
		switch step.step {
		case initState:
			state.Init()
		case mergeRequestsForIssue:
			if step.issue == "" {
				t.Fatalf("%s[%d].issue == \"\"", which, i)
			}
			mreqs := state.MergeRequestsForIssue(step.issue)
			if !reflect.DeepEqual(mreqs, step.mreqs) {
				t.Errorf("%s[%d]: MergeRequestsForIssue(%v) got %v want %v", which, i, step.issue,
					mreqs, step.mreqs)
			}
		case issuesForMergeRequest:
			if step.mrid == 0 {
				t.Fatalf("%s[%d].mrid == 0", which, i)
			}
			issues := state.IssuesForMergeRequest(step.mrid)
			if !reflect.DeepEqual(issues, step.issues) {
				t.Errorf("%s[%d]: IssuesForMergeRequest(%v) got %v want %v", which, i, step.mrid,
					issues, step.issues)
			}
		case setMergeRequestIssues:
			if step.mrid == 0 {
				t.Fatalf("%s[%d].mrid == 0", which, i)
			}
			state.SetMergeRequestIssues(step.mrid, step.issues)
		case commitsForIssue:
			if step.issue == "" {
				t.Fatalf("%s[%d].issue == \"\"", which, i)
			}
			commits := state.CommitsForIssue(step.issue)
			if !reflect.DeepEqual(commits, step.commits) {
				t.Errorf("%s[%d]: CommitsForIssue(%v) got %v want %v", which, i, step.issue,
					commits, step.commits)
			}
		case issuesForCommit:
			if step.cid == "" {
				t.Fatalf("%s[%d].cid == \"\"", which, i)
			}
			issues := state.IssuesForCommit(step.cid)
			if !reflect.DeepEqual(issues, step.issues) {
				t.Errorf("%s[%d]: IssuesForMergeRequest(%v) got %v want %v", which, i, step.cid,
					issues, step.issues)
			}
		case setCommitIssues:
			if step.cid == "" {
				t.Fatalf("%s[%d].cid == \"\"", which, i)
			}
			state.SetCommitIssues(step.cid, step.issues)
		case commitsForMergeRequest:
			if step.mrid == 0 {
				t.Fatalf("%s[%d].mrid == 0", which, i)
			}
			commits := state.CommitsForMergeRequest(step.mrid)
			if !reflect.DeepEqual(commits, step.commits) {
				t.Errorf("%s[%d]: CommitsForMergeRequest(%v) got %v want %v", which, i, step.mrid,
					commits, step.commits)
			}
		case setMergeRequestCommits:
			if step.mrid == 0 {
				t.Fatalf("%s[%d].mrid == 0", which, i)
			}
			state.SetMergeRequestCommits(step.mrid, step.commits)
		default:
			t.Fatalf("%s[%d].step: unexpected: %d", which, i, step.step)
		}
	}
}

func TestState(t *testing.T) {
	test1 := []testStep{
		{step: initState},
		{step: mergeRequestsForIssue, issue: "EX-1234"},
		{step: issuesForMergeRequest, mrid: 678},
		{step: setMergeRequestIssues, mrid: 678},
		{step: mergeRequestsForIssue, issue: "EX-1234"},
		{step: issuesForMergeRequest, mrid: 678},
		{step: setMergeRequestIssues, mrid: 678, issues: []string{"EX-1234"}},
		{step: mergeRequestsForIssue, issue: "EX-1234", mreqs: []int{678}},
		{step: issuesForMergeRequest, mrid: 678, issues: []string{"EX-1234"}},
		{step: setMergeRequestIssues, mrid: 678, issues: []string{"EX-5678"}},
		{step: mergeRequestsForIssue, issue: "EX-1234"},
		{step: mergeRequestsForIssue, issue: "EX-5678", mreqs: []int{678}},
		{step: issuesForMergeRequest, mrid: 678, issues: []string{"EX-5678"}},
	}
	test("test1", test1, t)

	test2 := []testStep{
		{step: initState},
		{step: setMergeRequestIssues, mrid: 10, issues: []string{"EX-1", "EX-2", "EX-3"}},
		{step: setMergeRequestIssues, mrid: 11, issues: []string{"EX-2", "EX-3", "EX-4"}},
		{step: setMergeRequestIssues, mrid: 12, issues: []string{"EX-3"}},
		{step: setMergeRequestIssues, mrid: 13},
		{step: mergeRequestsForIssue, issue: "EX-1", mreqs: []int{10}},
		{step: mergeRequestsForIssue, issue: "EX-2", mreqs: []int{10, 11}},
		{step: mergeRequestsForIssue, issue: "EX-3", mreqs: []int{10, 11, 12}},
		{step: mergeRequestsForIssue, issue: "EX-4", mreqs: []int{11}},
		{step: mergeRequestsForIssue, issue: "EX-5"},
		{step: issuesForMergeRequest, mrid: 10, issues: []string{"EX-1", "EX-2", "EX-3"}},
		{step: issuesForMergeRequest, mrid: 11, issues: []string{"EX-2", "EX-3", "EX-4"}},
		{step: issuesForMergeRequest, mrid: 12, issues: []string{"EX-3"}},
		{step: issuesForMergeRequest, mrid: 13},

		{step: setMergeRequestIssues, mrid: 11, issues: []string{"EX-2", "EX-5"}},
		{step: mergeRequestsForIssue, issue: "EX-1", mreqs: []int{10}},
		{step: mergeRequestsForIssue, issue: "EX-2", mreqs: []int{10, 11}},
		{step: mergeRequestsForIssue, issue: "EX-3", mreqs: []int{10, 12}},
		{step: mergeRequestsForIssue, issue: "EX-4"},
		{step: mergeRequestsForIssue, issue: "EX-5", mreqs: []int{11}},
		{step: issuesForMergeRequest, mrid: 10, issues: []string{"EX-1", "EX-2", "EX-3"}},
		{step: issuesForMergeRequest, mrid: 11, issues: []string{"EX-2", "EX-5"}},
		{step: issuesForMergeRequest, mrid: 12, issues: []string{"EX-3"}},

		{step: setMergeRequestIssues, mrid: 10, issues: []string{"EX-1", "EX-2"}},
		{step: mergeRequestsForIssue, issue: "EX-1", mreqs: []int{10}},
		{step: mergeRequestsForIssue, issue: "EX-2", mreqs: []int{10, 11}},
		{step: mergeRequestsForIssue, issue: "EX-3", mreqs: []int{12}},
		{step: mergeRequestsForIssue, issue: "EX-4"},
		{step: mergeRequestsForIssue, issue: "EX-5", mreqs: []int{11}},
		{step: issuesForMergeRequest, mrid: 10, issues: []string{"EX-1", "EX-2"}},
		{step: issuesForMergeRequest, mrid: 11, issues: []string{"EX-2", "EX-5"}},
		{step: issuesForMergeRequest, mrid: 12, issues: []string{"EX-3"}},
	}
	test("test2", test2, t)

	test3 := []testStep{
		{step: initState},
		{step: setCommitIssues, cid: "COMMIT-A", issues: []string{"EX-1", "EX-2", "EX-3"}},
		{step: setCommitIssues, cid: "COMMIT-B", issues: []string{"EX-1"}},
		{step: setCommitIssues, cid: "COMMIT-C", issues: []string{"EX-4"}},
		{step: setCommitIssues, cid: "COMMIT-D", issues: []string{"EX-5"}},
		{step: setCommitIssues, cid: "COMMIT-E", issues: []string{"EX-3", "EX-5"}},
		{step: setCommitIssues, cid: "COMMIT-F", issues: []string{"EX-3"}},
		{step: setCommitIssues, cid: "COMMIT-G"},
		{step: commitsForIssue, issue: "EX-1", commits: []string{"COMMIT-A", "COMMIT-B"}},
		{step: commitsForIssue, issue: "EX-2", commits: []string{"COMMIT-A"}},
		{step: commitsForIssue, issue: "EX-3",
			commits: []string{"COMMIT-A", "COMMIT-E", "COMMIT-F"}},
		{step: commitsForIssue, issue: "EX-4", commits: []string{"COMMIT-C"}},
		{step: commitsForIssue, issue: "EX-5", commits: []string{"COMMIT-D", "COMMIT-E"}},
		{step: commitsForIssue, issue: "EX-6"},
		{step: issuesForCommit, cid: "COMMIT-A", issues: []string{"EX-1", "EX-2", "EX-3"}},
		{step: issuesForCommit, cid: "COMMIT-B", issues: []string{"EX-1"}},
		{step: issuesForCommit, cid: "COMMIT-C", issues: []string{"EX-4"}},
		{step: issuesForCommit, cid: "COMMIT-D", issues: []string{"EX-5"}},
		{step: issuesForCommit, cid: "COMMIT-E", issues: []string{"EX-3", "EX-5"}},
		{step: issuesForCommit, cid: "COMMIT-F", issues: []string{"EX-3"}},
		{step: issuesForCommit, cid: "COMMIT-G"},

		{step: setCommitIssues, cid: "COMMIT-A"},
		{step: setCommitIssues, cid: "COMMIT-E", issues: []string{"EX-3", "EX-5"}},
		{step: commitsForIssue, issue: "EX-1", commits: []string{"COMMIT-B"}},
		{step: commitsForIssue, issue: "EX-2"},
		{step: commitsForIssue, issue: "EX-3", commits: []string{"COMMIT-E", "COMMIT-F"}},
		{step: commitsForIssue, issue: "EX-4", commits: []string{"COMMIT-C"}},
		{step: commitsForIssue, issue: "EX-5", commits: []string{"COMMIT-D", "COMMIT-E"}},
		{step: commitsForIssue, issue: "EX-6"},
		{step: issuesForCommit, cid: "COMMIT-A"},
		{step: issuesForCommit, cid: "COMMIT-B", issues: []string{"EX-1"}},
		{step: issuesForCommit, cid: "COMMIT-C", issues: []string{"EX-4"}},
		{step: issuesForCommit, cid: "COMMIT-D", issues: []string{"EX-5"}},
		{step: issuesForCommit, cid: "COMMIT-E", issues: []string{"EX-3", "EX-5"}},
		{step: issuesForCommit, cid: "COMMIT-F", issues: []string{"EX-3"}},
		{step: issuesForCommit, cid: "COMMIT-G"},
	}
	test("test3", test3, t)

	test4 := []testStep{
		{step: initState},
		{step: setMergeRequestCommits, mrid: 123,
			commits: []string{"COMMIT-A", "COMMIT-B", "COMMIT-E"}},
		{step: setMergeRequestCommits, mrid: 456},
		{step: setMergeRequestCommits, mrid: 789, commits: []string{"COMMIT-C", "COMMIT-D"}},
		{step: commitsForMergeRequest, mrid: 123,
			commits: []string{"COMMIT-A", "COMMIT-B", "COMMIT-E"}},
		{step: commitsForMergeRequest, mrid: 456},
		{step: commitsForMergeRequest, mrid: 789, commits: []string{"COMMIT-C", "COMMIT-D"}},

		{step: setMergeRequestCommits, mrid: 123},
		{step: setMergeRequestCommits, mrid: 456, commits: []string{"COMMIT-A", "COMMIT-B"}},
		{step: setMergeRequestCommits, mrid: 789, commits: []string{"COMMIT-Z"}},
		{step: setMergeRequestCommits, mrid: 1234, commits: []string{"COMMIT-Q"}},
		{step: commitsForMergeRequest, mrid: 123},
		{step: commitsForMergeRequest, mrid: 456, commits: []string{"COMMIT-A", "COMMIT-B"}},
		{step: commitsForMergeRequest, mrid: 789, commits: []string{"COMMIT-Z"}},
		{step: commitsForMergeRequest, mrid: 1234, commits: []string{"COMMIT-Q"}},
	}
	test("test4", test4, t)
}
